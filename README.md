# Music 320c Starter Code

The [Music 320c](https://ccrma.stanford.edu/courses/320c/)
assignment starter-code in this directory assumes you have installed

* [JUCE](https://juce.com),
* [Plugin GUI Magic (PGM)](https://github.com/ffAudio/foleys_gui_magic), and
* [Faust](https://faust.grame.fr).

JUCE and PGM only provide source code, while Faust must be installed on your computer (typically in `/usr/local/bin`).

<!-- JOS Fork of PGM: https://github.com/josmithiii/foleys_gui_magic -->

### Finding JUCE and PGM Modules in Projucer

Projucer project files (extension `.jucer`) contain pointers to
"modules" in your JUCE and PGM clones.  The starter-code projects
expect to find them in the directories `~/PGM/modules/` and
`~/JUCE/modules/`.  Since you might want to switch among various
versions of PGM and/or JUCE, it is convenient to have the _symbolic
links_ `~/PGM` and `~/JUCE` in your home directory.  You can create
such links as follows:

```
ln -s /absolute/path/to/PGM ~/PGM
ln -s /absolute/path/to/JUCE ~/JUCE
```

For more info on building and testing Projucer, see [Assignment
JUCE](https://ccrma.stanford.edu/courses/320c/assignments/JUCE/).
