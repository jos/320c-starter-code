/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

// Faust parameter IDs
static juce::ParameterID ampID {"amplitude", 1};
static juce::ParameterID atID {"attack", 1};
static juce::ParameterID dcID {"decay", 1};
static juce::ParameterID stID {"sustain", 1};
static juce::ParameterID rlID {"release", 1};
static juce::ParameterID oscTypeID {"type", 1};

static juce::Identifier oscilloscopeID { "oscilloscope" };

juce::AudioProcessorValueTreeState::ParameterLayout FaustSynthExample::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;

    auto adsrGroup = std::make_unique<juce::AudioProcessorParameterGroup>("adsr", "ADSR","|");
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(atID, "Attack", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.1));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(dcID, "Decay", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.25));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(stID, "Sustain", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.5));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(rlID, "Release", juce::NormalisableRange<float>(0, 5.0, 0.01), 1.0));
    layout.add(std::move(adsrGroup));
    auto typeParam = std::make_unique<juce::AudioParameterChoice>(oscTypeID, "Type", juce::StringArray("Sine", "Square", "Triangle", "Saw"), 0);
    layout.add(std::move(typeParam));
    auto ampParam = std::make_unique<juce::AudioParameterFloat>(ampID, "Amplitude", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.1);
    layout.add(std::move(ampParam));
    return layout;
}
void FaustSynthExample::addPresetNameToTreeStateIfNotThere() {
  // Illustrate how to add any property to the treeState,
  // so that the DAW will include that with parameter settings when it saves the plugin state with the DAW project.
  // A common use case is preset-name _within_ the plugin:
  juce::Value presetNameValue = treeState.state.getPropertyAsValue("presetName", /* UndoManager* */ nullptr, /* updateSynchronously */ true);
  juce::String presetNameFound = presetNameValue.toString();
  DBG("Preset name in treeState is currently `" << presetNameFound << "'");
  if (presetName != presetNameFound) {
    DBG("Setting presetName to `" << presetName << "'");
    treeState.state.setProperty("presetName", presetName, nullptr);
  }
}

//==============================================================================
FaustSynthExample::FaustSynthExample()
     :
#ifndef JucePlugin_PreferredChannelConfigurations
       MagicProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
      treeState (*this, nullptr, JucePlugin_Name, createParameterLayout())
{
    preparedToPlay = false;

    // Synthesizer initialization
    synth.clearSounds();
    synth.addSound(new FaustSound());
    for (int nVoices = 0; nVoices < NUM_VOICES; ++nVoices)
    {
        synth.addVoice(new FaustVoice());
    }

    // Add listeners to our JUCE parameters
    treeState.addParameterListener(ampID.getParamID(), this);

    // Faust parameters use parameterChanged():
    treeState.addParameterListener(atID.getParamID(), this);
    treeState.addParameterListener(dcID.getParamID(), this);
    treeState.addParameterListener(stID.getParamID(), this);
    treeState.addParameterListener(rlID.getParamID(), this);
    treeState.addParameterListener(oscTypeID.getParamID(), this);

    // Get amplitude initial value
    amplitude = treeState.getParameter(ampID.getParamID())->getValue();
    // Add PGM oscilloscope to our synthesizer
    oscilloscope = magicState.createAndAddObject<foleys::MagicOscilloscope>(oscilloscopeID, 0);

    // Load GUI
    magicState.setGuiValueTree(BinaryData::Layout_HW3_xml, BinaryData::Layout_HW3_xmlSize);

    addPresetNameToTreeStateIfNotThere(); // Example of adding a new property to the treeState
}

FaustSynthExample::~FaustSynthExample()
{
}

//==============================================================================
const juce::String FaustSynthExample::getName() const
{
    return JucePlugin_Name;
}

bool FaustSynthExample::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool FaustSynthExample::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool FaustSynthExample::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double FaustSynthExample::getTailLengthSeconds() const
{
    return treeState.getParameter(rlID.getParamID())->getValue();
}

int FaustSynthExample::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int FaustSynthExample::getCurrentProgram()
{
    return 0;
}

void FaustSynthExample::setCurrentProgram (int index)
{
}

const juce::String FaustSynthExample::getProgramName (int index)
{
    return {};
}

void FaustSynthExample::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================

void FaustSynthExample::postSetStateInformation() { // Called by PGM when DAW/Host is setting our state
  // All parameters were set to their saved values in setStateInformation(), handled by PGM, who has now called this.
  // Our added presetName property tells us which _internal_ preset was modified to get those parameters.
  // Note that there is only _one_ set of parameters, so all internal presets are simply names for
  // various settings of those parameters.
  addPresetNameToTreeStateIfNotThere(); // Only "not there" when loading an old plugin state having no presetName property
}

//==============================================================================
void FaustSynthExample::prepareToPlay (double sampleRate, int samplesPerBlock) // FIXME JOS
{
    // juce::ignoreUnused(samplesPerBlock);
    synth.setCurrentPlaybackSampleRate(sampleRate);
    magicState.prepareToPlay (sampleRate, samplesPerBlock); // required for MagicPlotSource and such
    preparedToPlay = true;
}

void FaustSynthExample::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    preparedToPlay = false;
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool FaustSynthExample::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void FaustSynthExample::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    if (not preparedToPlay)
      return;

    juce::ScopedNoDenormals noDenormals;

    int nSamples = buffer.getNumSamples();

    buffer.clear(); // Audio buffers may be uninitialized.
                    // Clear now since Voices only add to the buffer when they're active.

    // check the magic state MidiBuffer for PGM Keyboard inputs
    magicState.processMidiBuffer(midiMessages, nSamples);
    // tell synthesizer to render the block
    synth.renderNextBlock(buffer, midiMessages, 0, nSamples);
    // apply overall gain based on amplitude
    buffer.applyGain(amplitude);
    // send buffer to plot in oscilloscope
    oscilloscope->pushSamples (buffer);
}

void FaustSynthExample::parameterChanged (const juce::String& param, float value)
{
    if (param == ampID.getParamID())
    {
      amplitude = value;
    } else
    if (param == atID.getParamID())
    {
        for(int nVoice=0; nVoice < NUM_VOICES; ++nVoice)
        {
            FaustVoice* synthVoice = dynamic_cast<FaustVoice *>(synth.getVoice(nVoice));
          if (synthVoice != nullptr)
            synthVoice->setParam("attack", value);
          else
            fprintf(stderr,"*** NULL SYNTH VOICE\n");
        }
    } else
    if (param == dcID.getParamID())
    {
        for(int nVoice=0; nVoice < NUM_VOICES; ++nVoice)
        {
            FaustVoice* synthVoice = dynamic_cast<FaustVoice *>(synth.getVoice(nVoice));
            synthVoice->setParam("decay", value);
        }
    } else
    if (param == stID.getParamID())
    {
        for(int nVoice=0; nVoice < NUM_VOICES; ++nVoice)
        {
            FaustVoice* synthVoice = dynamic_cast<FaustVoice *>(synth.getVoice(nVoice));
            synthVoice->setParam("sustain", value);
        }
    } else
    if (param == rlID.getParamID())
    {
        for(int nVoice=0; nVoice < NUM_VOICES; ++nVoice)
        {
            FaustVoice* synthVoice = dynamic_cast<FaustVoice *>(synth.getVoice(nVoice));
            synthVoice->setParam("release", value);
        }
    } else
    if (param == oscTypeID.getParamID())
    {
        for(int nVoice=0; nVoice < NUM_VOICES; ++nVoice)
        {
            FaustVoice* synthVoice = dynamic_cast<FaustVoice *>(synth.getVoice(nVoice));
            synthVoice->setParam("type", value);
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new FaustSynthExample();
}
