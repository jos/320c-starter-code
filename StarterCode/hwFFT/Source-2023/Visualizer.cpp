/*
  ==============================================================================
    
  MUSIC 320C - ASSIGNMENT 1 STARTER CODE
  VISUALIZER.CPP
  SPRING 2022 

  COPYRIGHT (C) 2022 CENTER FOR COMPUTER RESEARCH IN MUSIC AND ACOUSTICS

  ==============================================================================
*/
#include "Visualizer.h"

// Constructor requires pointer to data array and data array size
Visualizer::Visualizer(float * dataIn, int dataSizeIn):
  data(dataIn),
  dataSize(dataSizeIn)
{
}

// Set X write the 'x' pointer. It is up to the user to ensure
// x matches dataSize
void Visualizer::setX(float * newX)
{
  x = newX;
}

// Update calls the parent resetLastDataFlag() method to update the plot
void Visualizer::update()
{
  resetLastDataFlag();
}

// createPlotPaths updates paths 
void Visualizer::createPlotPaths(juce::Path &path, juce::Path &filledPath, juce::Rectangle<float> bounds,
                                 foleys::MagicPlotComponent &component)
{
  // YOUR CODE GOES HERE
  // Modify the arguments as needed
}
