/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
juce::AudioProcessorValueTreeState::ParameterLayout
PGMAudioProcessor::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;
    return layout;
}

//==============================================================================
PGMAudioProcessor::PGMAudioProcessor()
  : // One or two initializations:
#ifndef JucePlugin_PreferredChannelConfigurations
  foleys::MagicProcessor ( BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
                           .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
#endif
                           .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
#endif
                           ),
#endif
  treeState (*this, nullptr, ProjectInfo::projectName, createParameterLayout())
{ // Constructor body begins:
  // We have access to our parent's magicState instance,
  // foleys::MagicProcessor, which we can use to set the current GUI
  // from our stored BinaryData default:
  magicState.setGuiValueTree(BinaryData::Layout_xml, BinaryData::Layout_xmlSize);
  // For more on the magicState and parent PGM classes, see the ref doc:
  // https://ccrma.stanford.edu/~jos/juce_modules/classMagicProcessorState.html
  // https://ccrma.stanford.edu/~jos/juce_modules/classMagicProcessor.html
}

PGMAudioProcessor::~PGMAudioProcessor()
{
}

//==============================================================================
const juce::String PGMAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool PGMAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool PGMAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool PGMAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double PGMAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int PGMAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int PGMAudioProcessor::getCurrentProgram()
{
    return 0;
}

void PGMAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String PGMAudioProcessor::getProgramName (int index)
{
    return {};
}

void PGMAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void PGMAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void PGMAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool PGMAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void PGMAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer (channel);

        // ..do something to the data...
    }
}

//==============================================================================
//bool PGMAudioProcessor::hasEditor() const
//{
//    return true; // (change this to false if you choose to not supply an editor)
//}
//
//juce::AudioProcessorEditor* PGMAudioProcessor::createEditor()
//{
//    return new PGMAudioProcessorEditor (*this);
//}

//==============================================================================
#if 0 // Let PGM handle this:

void PGMAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void PGMAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

#endif

void PGMAudioProcessor::parameterChanged(const juce::String &parameterID, float newValue)
{}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new PGMAudioProcessor();
}

