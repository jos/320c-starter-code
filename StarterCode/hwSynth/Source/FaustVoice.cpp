#include "FaustVoice.h"

FaustVoice::FaustVoice()
{
}

FaustVoice::~FaustVoice()
{
}

void FaustVoice::startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound* sound, int currentPitchWheelPosition)
{
}

void FaustVoice::stopNote(float /*velocity*/, bool allowTailOff)
{
}

void FaustVoice::renderNextBlock(juce::AudioBuffer<float> &outputBuffer, int startSample, int numSamples)
{
  // Render audio here with compute code!
  int nChannelsOut = outputBuffer.getNumChannels();
}
