declare name "monoSource";
declare description "Monophonic midi waveform generator";

import("stdfaust.lib");

process = monoSource;

monoSource = env*wave
with
{
    // Note Control Parameters
    vel = nentry("velocity", 1, 0, 1, 0.01);
    gate = button("gate");
    freqHz = ba.midikey2hz(int(nentry("midiNote", 60, 36, 96, 1)));

    // Waveform Control Parameter
    select = int(nentry("type", 0, 0, 3, 1));

    // Envelope Control Parameters
    at = nentry("attack", 0.1, 0, 1, 0.01);
    dt = nentry("decay", 0.25, 0, 1, 0.01);
    sl = nentry("sustain", 0.5, 0, 1, 0.01);
    rt = nentry("release", 1, 0, 5, 0.01);
    env = vel * en.adsre(at, dt, sl, rt, gate);

    // Waveforms
    sqr = os.square(freqHz);
    sine = os.osc(freqHz);
    tri = os.triangle(freqHz);
    saw = os.sawtooth(freqHz);
    // Switch for waveforms
    wave = (sine * (select == 0) + sqr * (select == 1) + tri * (select == 2) + saw * (select == 3));
};