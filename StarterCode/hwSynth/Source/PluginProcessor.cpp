/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

// Faust parameter IDs
static juce::ParameterID ampID {"amplitude", 1};
static juce::ParameterID atID {"attack", 1};
static juce::ParameterID dcID {"decay", 1};
static juce::ParameterID stID {"sustain", 1};
static juce::ParameterID rlID {"release", 1};
static juce::ParameterID oscTypeID {"type", 1};

static juce::Identifier oscilloscopeID { "oscilloscope" };

juce::AudioProcessorValueTreeState::ParameterLayout FaustSynthStarter::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;

    auto adsrGroup = std::make_unique<juce::AudioProcessorParameterGroup>("adsr", "ADSR","|");
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(atID, "Attack", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.1));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(dcID, "Decay", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.25));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(stID, "Sustain", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.5));
    adsrGroup->addChild(std::make_unique<juce::AudioParameterFloat>(rlID, "Release", juce::NormalisableRange<float>(0, 5.0, 0.01), 1.0));
    layout.add(std::move(adsrGroup));
    auto typeParam = std::make_unique<juce::AudioParameterChoice>(oscTypeID, "Type", juce::StringArray("Sine", "Square", "Triangle", "Saw"), 0);
    layout.add(std::move(typeParam));
    auto ampParam = std::make_unique<juce::AudioParameterFloat>(ampID, "Amplitude", juce::NormalisableRange<float>(0, 1.0, 0.01), 0.1);
    layout.add(std::move(ampParam));
    return layout;
}
//==============================================================================
FaustSynthStarter::FaustSynthStarter()
     :
#ifndef JucePlugin_PreferredChannelConfigurations
       MagicProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
      treeState (*this, nullptr, JucePlugin_Name, createParameterLayout())
{
    // Initialize synthesizer here:

    // Add listeners to our JUCE parameters
    treeState.addParameterListener(ampID.getParamID(), this);

    // Faust parameters use parameterChanged():
    treeState.addParameterListener(atID.getParamID(), this);
    treeState.addParameterListener(dcID.getParamID(), this);
    treeState.addParameterListener(stID.getParamID(), this);
    treeState.addParameterListener(rlID.getParamID(), this);
    treeState.addParameterListener(oscTypeID.getParamID(), this);

    // Get amplitude initial value
    amplitude = treeState.getParameter(ampID.getParamID())->getValue();
    // Add PGM oscilloscope to our synthesizer
    oscilloscope = magicState.createAndAddObject<foleys::MagicOscilloscope>(oscilloscopeID, 0);

    // Load GUI
    magicState.setGuiValueTree(BinaryData::Layout_HW3_xml, BinaryData::Layout_HW3_xmlSize);
}

FaustSynthStarter::~FaustSynthStarter()
{
}

//==============================================================================
const juce::String FaustSynthStarter::getName() const
{
    return JucePlugin_Name;
}

bool FaustSynthStarter::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool FaustSynthStarter::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool FaustSynthStarter::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double FaustSynthStarter::getTailLengthSeconds() const
{
    return treeState.getParameter(rlID.getParamID())->getValue();
}

int FaustSynthStarter::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int FaustSynthStarter::getCurrentProgram()
{
    return 0;
}

void FaustSynthStarter::setCurrentProgram (int index)
{
}

const juce::String FaustSynthStarter::getProgramName (int index)
{
    return {};
}

void FaustSynthStarter::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void FaustSynthStarter::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Don't forget to sample rate initialize your synthesizer!
    magicState.prepareToPlay (sampleRate, samplesPerBlock); // required for MagicPlotSource and such
}

void FaustSynthStarter::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool FaustSynthStarter::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void FaustSynthStarter::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;

    int nSamples = buffer.getNumSamples();

    buffer.clear(); // Audio buffers may be uninitialized.
                    // Clear now since Voices only add to the buffer when they're active.

    // check the magic state MidiBuffer for PGM Keyboard inputs
    magicState.processMidiBuffer(midiMessages, nSamples);
    // Call Synthesizer and tell it to renderBlock

    // apply overall gain based on amplitude
    buffer.applyGain(amplitude);
    // send buffer to plot in oscilloscope
    oscilloscope->pushSamples (buffer);
}

void FaustSynthStarter::parameterChanged (const juce::String& param, float value)
{
    if (param == ampID.getParamID())
    {
      amplitude = value;
    }

    // Add Faust Synthesizer Parameter Handling
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new FaustSynthStarter();
}
